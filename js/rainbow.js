function Ribbon( context )
{
    this.init( context );
}

Ribbon.prototype =
{
    context: null,

    mouseX: null, mouseY: null,

    painters: null,

    interval: null,

    init: function( context )
    {
        var scope = this;
        var BRUSH_SIZE = 1;
        var BRUSH_PRESSURE = 1;
        
        this.context = context;
        this.context.globalCompositeOperation = 'source-over';

        this.mouseX = window.innerWidth;
        this.mouseY = 200;
        
        this.maxBrushSize = Math.round( Math.random()*10+5 );

        this.painters = new Array();
        var ease;
        for (var i = 0; i < 13; i++)
        {
            ease = 0.7 + i*0.01;
            this.painters.push({ dx: -50, dy: 225 - i*10+60, ax: 0, ay: 0, div: 0.01, ease: ease });
        }
        
        this.interval = setInterval( update, 1000/60 );
        
        function update()
        {
            var i;
            
            this.context.lineWidth = BRUSH_SIZE*10;     
            this.context.lineCap = 'round';         
            //this.context.strokeStyle = "rgba(" + this.COLOR[0] + ", " + this.COLOR[1] + ", " + this.COLOR[2] + ", " + 0.15 * this.BRUSH_PRESSURE + ")";
            
            var colors = [
                        [216, 48, 32],
                        
                            [234, 96, 16],
                        
                        [252, 144, 0],
                        
                            [253, 186, 0],  
                        
                        [255, 228, 0],
                        
                            [186, 213, 25],
                        
                        [116, 202, 51],
                        
                            [66, 193, 116],
                        
                        [23, 183, 182],
                        
                            [12, 127, 181],
                        [0, 72, 179],
                            [122, 36, 187],
                        [245, 0, 196]

                        ];
            
            var i = 0;
            var painterLength = scope.painters.length;
            var painter;
            var context = scope.context;
            for (; i < painterLength; i++)
            {
                this.context.strokeStyle = "rgba(" + colors[i][0] + ", " + colors[i][1] + ", " + colors[i][2] + ", " + 1 * BRUSH_PRESSURE + ")";
                
                painter = scope.painters[i];
                
                context.beginPath();
                context.moveTo(painter.dx, painter.dy);     
                
                var before = [painter.dx, painter.dy];

                painter.dx -= painter.ax = (painter.ax + (painter.dx - scope.mouseX) * painter.div) * painter.ease;
                painter.dy -= painter.ay = (painter.ay + (painter.dy - scope.mouseY) * painter.div) * painter.ease;
                
                var deltaX = Math.abs(painter.dx - before[0]);
                var deltaY = Math.abs(painter.dy - before[1]);
                var length = Math.max(1, Math.sqrt(deltaX*deltaX + deltaY*deltaY));
                this.context.lineWidth = Math.min(10, Math.round(BRUSH_SIZE*10 * length/10));
                //this.context.lineWidth = 10;
                
                context.lineTo(painter.dx, painter.dy);
                context.stroke();
            }
        }
    },
    
    destroy: function()
    {
        clearInterval(this.interval);
    },

    strokeStart: function( mouseX, mouseY )
    {
        this.mouseX = mouseX;
        this.mouseY = mouseY;
        
        var i;
        var length = this.painters.length;
        var painter;

        for (; i < length; i++)
        {
            painter = this.painters[i];
            
            painter.dx = mouseX;
            painter.dy = mouseY;
        }

        this.shouldDraw = true;
    },

    stroke: function( mouseX, mouseY )
    {
        this.mouseX = mouseX;
        this.mouseY = mouseY;
    },

    strokeEnd: function()
    {
    
    }
}


function RainbowCanvas(){
    var SCREEN_WIDTH;
    var SCREEN_HEIGHT;
    var BRUSH_SIZE;
    var BRUSH_PRESSURE;
    var COLOR;
    var BACKGROUND_COLOR;
    var brush;
    var saveTimeOut;
    var wacom;
    var i;
    var mouseX;
    var mouseY;
    var container;
    var canvas;
    var context;
    var USER_AGENT;
}

RainbowCanvas.prototype.init = function( canvas ) {
    this.SCREEN_WIDTH = window.innerWidth;
    this.SCREEN_HEIGHT = window.innerHeight;
    this.BRUSH_SIZE = 1;
    this.BRUSH_PRESSURE = 1;
    this.COLOR = [0, 0, 0];
    this.BACKGROUND_COLOR = [173, 222, 229];
    this.mouseX = 0;
    this.mouseY = 0;
    this.USER_AGENT = navigator.userAgent.toLowerCase();
    
    
    var holder = document.getElementById('rainbowCanvas');
    if( !holder ) return;
    
    holder.style.backgroundRepeat = 'no-repeat';
    holder.style.backgroundPosition = 'center center';  
    
    container = document.createElement('div');
    holder.appendChild(container);

    this.canvas = canvas;
    //canvas = document.createElement("canvas");
    canvas.width = this.SCREEN_WIDTH;
    canvas.height = this.SCREEN_HEIGHT;
    container.appendChild(canvas);
    
    context = canvas.getContext("2d");
    this.brush = eval('new Ribbon(context)');
    
    window.addEventListener('mousemove', this.onWindowMouseMove, false);
    window.addEventListener('resize', this.onWindowResize, false);
    
    this.onWindowResize(null);
    this.onCanvasMouseDown(null);
}

RainbowCanvas.prototype.onWindowMouseMove = function( event ) {
    this.mouseX = event.clientX;
    this.mouseY = event.clientY;
}

RainbowCanvas.prototype.onWindowResize = function( event ) {
    this.SCREEN_WIDTH = window.innerWidth;
    this.SCREEN_HEIGHT = window.innerHeight;
    
    //rainbowCanvas.canvas.width = this.SCREEN_WIDTH;
    rainbowCanvas.canvas.height = this.SCREEN_HEIGHT;
}

RainbowCanvas.prototype.onCanvasMouseDown = function( event ) {
    window.addEventListener('mousemove', this.onCanvasMouseMove, false);
}

RainbowCanvas.prototype.onCanvasMouseMove = function( event ) { 
    rainbowCanvas.brush.stroke( event.clientX, event.clientY );
}

// Only create rainbow canvas if browser supports the canvas tag.
var rainbowCanvas;
function startRainbow(){
    var canvas = document.createElement('canvas');
    var isFirefox = (/firefox|minefield/i.test(navigator.userAgent));
    //isFirefox = false;
    if (!!canvas.getContext && !isFirefox) {
        rainbowCanvas = new RainbowCanvas();
        rainbowCanvas.init(canvas);
    }
}

Event.observe(window, 'load', function() { startRainbow() });